package ut.com.example.plugins.tutorial.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class MyWeborkActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //MyWeborkAction testClass = new MyWeborkAction();

        throw new Exception("MyWeborkAction has no tests!");

    }

}
