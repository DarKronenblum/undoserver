package ut.com.example.plugins.tutorial.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class UndoPluginUserTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //UndoPluginUser testClass = new UndoPluginUser();

        throw new Exception("UndoPluginUser has no tests!");

    }

}
