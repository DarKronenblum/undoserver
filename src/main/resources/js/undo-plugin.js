

$(document).ready(function(){
        var url=document.URL;
        var myFlag;

        if (url.includes("#undo=")) {//after undo

                if (url.includes("#undo=1")) {//success
                        console.log('success: ' + url);
                        myFlag = AJS.flag({
                                type: 'success',
                                body: 'Action undo.',
                        });
                } else if (url.includes("#undo=0&msg=")) {//failure

                        var errorMsg = url.split("undo=0&msg=")[1];
                        errorMsg = errorMsg.replace("undo=0&msg=", '');
                        errorMsg = errorMsg.split("-").join(" ");

                        myFlag = AJS.flag({
                                type: 'error',
                                body: errorMsg,
                        });
                }


                document.location.hash = '';
        }
        setTimeout(function () {
                myFlag.close();
                return;
        },5000);

});

