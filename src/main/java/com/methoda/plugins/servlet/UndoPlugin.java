//All Rights Reserved to Methoda Computers Ltd. 2019 Israel. You cannot use, reuse or copy the code of this addon without the written concent of Methoda.

package com.methoda.plugins.servlet;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.managedconfiguration.ConfigurationItemAccessLevel;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItem;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemBuilder;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
@Named
public class UndoPlugin extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(UndoPlugin.class);

    private JiraAuthenticationContext authenticationContext;
    private PageBuilderService pageBuilderService;

    // private  PluginLicenseManager pluginLicenseManager;
    private  ComponentAccessor componentAccessor;
    @ComponentImport
    private final PluginLicenseManager pluginLicenseManager;

    private static final String UNDO_ISSUE_TEMPLATE = "/templates/UndoErrorScreen.vm";
    @Inject
    public UndoPlugin(PluginLicenseManager pluginLicenseManager,
                      @ComponentImport JiraAuthenticationContext authenticationContext) {
        this.pluginLicenseManager = pluginLicenseManager;
        this.authenticationContext=authenticationContext;


    }
    @Override

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String undoStatusCode="0";
        String undoErrorMsg="";
        try {

                        log.warn("start");
                        Boolean success = true;
                        Map<String, Object> context = new HashMap<>();

                        String issueKey = req.getParameter("key");
                        context.put("issue", req.getParameter("key"));
                        Issue issue = ComponentAccessor.getIssueManager().getIssueObject(issueKey);
                        if (issue != null) {//valid issue
                            ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();
                            List<ChangeHistory> issueChanges = changeHistoryManager.getChangeHistories(issue);
                            CustomField undoCf = getUndoCustomField("Action Undo Custom Field");
                            List<String> groupIdsArr = new ArrayList<String>();
                            if (issue.getCustomFieldValue(undoCf) != null) {//valid cf
                                groupIdsArr = new ArrayList<>(Arrays.asList(issue.getCustomFieldValue(undoCf).toString().replaceAll("[\\[\\] ]", "").split(",")));
                            }
                            if (issueChanges.size() > 0) {//there are actions on the issue
                                for (int i = 1; i <= issueChanges.size(); i++) {
                                    List<org.ofbiz.core.entity.GenericValue> lastHistoryItems = issueChanges.get(issueChanges.size() - i).getChangeItems();
                                    if (!lastHistoryItems.isEmpty()) {//there is value in the last history item
                                        String groupId = lastHistoryItems.get(0).getAllFields().get("group").toString();
                                        groupId = groupId.trim();
                                        if (!groupIdsArr.contains(groupId)) {//not undo action and not action that been undo
                                            JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
                                            ApplicationUser currentUser = authenticationContext.getLoggedInUser();
                                            log.warn("lastHistoryItems: " + lastHistoryItems.toString());
                                            if (lastHistoryItems.toString().contains("WorklogId")) {//log work
                                                Long remaining = 0L, worklogId = 0L;
                                                Integer startIndex = 0, endIndex = 0;
                                                String historyString = lastHistoryItems.toString();
                                                String startString = "[field,timeestimate][oldstring,";
                                                String endString = "][newstring,";
                                                startIndex = historyString.indexOf(startString) + startString.length();
                                                endIndex = historyString.indexOf(endString, startIndex);
                                                if (startIndex > 0 && endIndex > 0 && startIndex < endIndex) {
                                                    remaining = Long.parseLong(historyString.substring(startIndex, endIndex));
                                                }
                                                startString = "[field,WorklogId][oldstring,";
                                                startIndex = historyString.indexOf(startString) + startString.length();
                                                endIndex = historyString.indexOf(endString, startIndex);
                                                if (startIndex > 0 && endIndex > 0 && startIndex < endIndex) {
                                                    worklogId = Long.parseLong(historyString.substring(startIndex, endIndex));
                                                }
                                                WorklogManager worklogManager = ComponentAccessor.getWorklogManager();
                                                Worklog worklog = worklogManager.getById(worklogId);
                                                if (worklog != null) {
                                                    worklogManager.delete(currentUser, worklog, remaining, true);
                                                }
                                                break;
                                            } else {
                                                success = undoHistoryGroupActions(lastHistoryItems, issue, currentUser, groupIdsArr, undoCf);
                                                log.error("success: " + success);
                                                undoStatusCode="1";
                                                if (success == true) {
                                                    break;
                                                }
                                            }
                                        } else {
                                            undoErrorMsg="No action to undo";
                                            log.error("no action to undo! group Ids : " + groupIdsArr + "undo Group Id " + groupId);

                                        }
                                    } else {
                                        undoErrorMsg="Last History Item is empty- can't undo";
                                        log.error("Last History Item is empty- can't undo");
                                    }
                                }
                            } else {
                                undoErrorMsg="No action to undo";
                                log.error("No action to undo!!");
                            }
                        }

        }
        catch (UnsupportedOperationException error){
            log.error(error.toString());
        }
        String sourceUrl = req.getHeader("referer");
        sourceUrl=sourceUrl.split("#undo")[0];
        resp.sendRedirect(sourceUrl+"#undo="+undoStatusCode+"&msg="+undoErrorMsg.replaceAll(" ","-"));

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    }
    /////////////////////////////////////////////////////////////////////////


    public CustomField getUndoCustomField(String customFieldName) {
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        CustomField undoCf = null;
        Collection<CustomField> customFields = customFieldManager.getCustomFieldObjectsByName(customFieldName);
        if (customFields.isEmpty()) {
            log.warn("creating new custom field");
            CustomFieldType stringCFType = customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textarea");
            CustomFieldSearcher textAreaSearcher = customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher");
            List<JiraContextNode> contexts = new ArrayList<JiraContextNode>();
            contexts.add(GlobalIssueContext.getInstance());
            List<IssueType> issueTypes =null;
            try {
                undoCf = customFieldManager.createCustomField(customFieldName, "Undo Plugin Custom Field- don't Edit or Delete", stringCFType, textAreaSearcher, contexts, issueTypes);
                FieldConfigSchemeManager fieldConfigSchemeManager= componentAccessor.getFieldConfigSchemeManager();
                fieldConfigSchemeManager.createDefaultScheme(undoCf,contexts,issueTypes);
            } catch (GenericEntityException e) {
                log.error("GenericEntityException: "+e);
            }
        } else {
            undoCf = customFields.iterator().next();
        }
        log.warn("undoCf: "+undoCf);
        ManagedConfigurationItemService managedConfigurationItemService = ComponentAccessor.getComponent(ManagedConfigurationItemService.class);

        ManagedConfigurationItem managedField = managedConfigurationItemService.getManagedCustomField( undoCf );
        if ( managedField != null ) {
            ManagedConfigurationItemBuilder builder = ManagedConfigurationItemBuilder.builder( managedField );
            builder.setManaged( true );
            builder.setConfigurationItemAccessLevel( ConfigurationItemAccessLevel.LOCKED );
            managedField = builder.build();
            managedConfigurationItemService.updateManagedConfigurationItem( managedField );
        }
        return undoCf;

    }
    public Boolean handleLinks(Issue destIssue,Issue issue,String oldstring,ApplicationUser currentUser,Boolean method){
        Boolean res=true;
        //true=add, false=remove
        String linkString=oldstring.replace("This issue ","").replace(destIssue.getKey(),"").trim();
        IssueLinkTypeManager issueLinkTypeManager=ComponentAccessor.getComponent(IssueLinkTypeManager.class);
        Collection<IssueLinkType> issueLinkTypes = issueLinkTypeManager.getIssueLinkTypes();
        IssueLinkType selectedLinkType=null;
        String direction="";
        for (IssueLinkType issueLinkType:issueLinkTypes){
            if (linkString.contains(issueLinkType.getInward())){
                selectedLinkType=issueLinkType;
                direction="Inward";
            }
            if (linkString.contains(issueLinkType.getOutward())){
                selectedLinkType=issueLinkType;
                direction="Outward";
            }
        }
        IssueLinkManager issueLinkManager=componentAccessor.getIssueLinkManager();
        if (destIssue!=null && direction!="" && selectedLinkType!=null) {
            log.warn("selectedLinkType: " + selectedLinkType.getName() + " direction:" + direction + "destIssueKey: " + destIssue.getKey());
            if (direction=="Inward"){
                try {
                    if (method==false) {
                        issueLinkManager.createIssueLink(destIssue.getId(), issue.getId(), selectedLinkType.getId(), null, currentUser);
                    }
                    else{
                        IssueLink selectedLink =issueLinkManager.getIssueLink(destIssue.getId(), issue.getId(), selectedLinkType.getId());
                        if (selectedLink != null){
                            issueLinkManager.removeIssueLink(selectedLink, currentUser);
                        }
                    }
                } catch (CreateException e) {
                    log.error("CreateException: "+e);
                    res=false;
                }
            }
            else {
                try {
                    if (method==false) {
                        issueLinkManager.createIssueLink(issue.getId(), destIssue.getId(), selectedLinkType.getId(), null, currentUser);
                    }
                    else {
                        IssueLink selectedLink = issueLinkManager.getIssueLink(issue.getId(), destIssue.getId(), selectedLinkType.getId());
                        if (selectedLink != null){
                            issueLinkManager.removeIssueLink(selectedLink, currentUser);
                        }
                    }
                } catch (CreateException e) {
                    log.error("CreateException: "+e);
                    res=false;
                }
            }
        }
        return res;

    }
    public Long [] updatedComponentsVersions(Long [] curValues, String newstring, String oldstring){

        log.warn("updatedComponentsVersions : " + curValues);
        List<Long> newIds=new ArrayList<Long>();
        for (Long ver:curValues) {
            log.warn("ver : " + ver);
            Long num=0L;
            if (newstring!="") {//version added
                num = Long.parseLong(newstring);
            }
            Long diff=num-ver;
            if (diff!=0) {//version is different than the
                newIds.add( ver);
            }
        }
        if (oldstring!="") {//ver removed
            newIds.add(Long.parseLong(oldstring));
        }
        Long [] array = new Long[newIds.size()];
        newIds.toArray(array); // fill the array
        log.warn("newIds : " + newIds);
        log.warn("array : " + array);
        return array;
    }
    public CustomField getRelevantCF(Issue issue,String fieldName,String newVal ) {
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        Collection<CustomField> cfList = customFieldManager.getCustomFieldObjectsByName(fieldName);
        CustomField relevantaCF = cfList.iterator().next();

        if (!cfList.isEmpty()) {
            for (CustomField cf : cfList) {
                if (cf.getValue(issue) != null && newVal != null) {
                    if (cf.getValue(issue).toString() == newVal) {
                        relevantaCF = cf;
                    }
                }
            }
        }
        return relevantaCF;
    }
    public Boolean undoHistoryGroupActions(List<org.ofbiz.core.entity.GenericValue> lastHistoryItems,Issue issue,ApplicationUser currentUser ,List<String> groupIdsArr,CustomField undoCf ) {
        Boolean success = true;
        Long[] fixVerArray = getIds(null, issue.getFixVersions());
        Long[] affectedVerArray = getIds(null, issue.getAffectedVersions());
        Long[] compArray = getIds(issue.getComponents(), null);
        Long[] fixVerArrayOrig=fixVerArray;
        Long[] affectedVerArrayOrig=affectedVerArray;
        Long[] compArrayOrig=compArray;

        IssueService issueService = ComponentAccessor.getIssueService();
        ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();
        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        for (org.ofbiz.core.entity.GenericValue historyItem : lastHistoryItems) {//every item in the last history group
            log.warn("historyItem: " + historyItem);
            Map<String, Object> data = historyItem.getAllFields();
            try {
                String fieldName = "", oldstring = "", newstring = "", fieldtype = "";
                log.warn("data: " + data);
                fieldName = data.get("field").toString();
                oldstring = "";
                if (data.get("oldvalue") != null) {
                    oldstring = data.get("oldvalue").toString();
                } else {
                    if (data.get("oldstring") != null) {
                        oldstring = data.get("oldstring").toString();
                    }
                }
                newstring = "";
                if (data.get("newvalue") != null) {
                    newstring = data.get("newvalue").toString();
                } else {
                    if (data.get("newstring") != null) {
                        newstring = data.get("newstring").toString();
                    }
                }
                fieldtype = data.get("fieldtype").toString();
                if (fieldtype.toLowerCase().contains("custom")) {
                    CustomField cf = getRelevantCF(issue, fieldName, newstring);
                    String cfType = cf.getCustomFieldType().getName();
                    if (oldstring == "") {
                        issueInputParameters.addCustomFieldValue(cf.getId(), "");
                    } else {
                        switch (cfType) {
                            case "Date Picker":
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date date = format.parse(oldstring);
                                issueInputParameters.addCustomFieldValue(cf.getId(), new SimpleDateFormat("d/MMM/yy hh:mm a", Locale.ENGLISH).format(date));
                                break;
                            case "Date Time Picker":
                                format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.ENGLISH);
                                date = format.parse(oldstring);
                                Timestamp timeStamp = new java.sql.Timestamp(date.getTime());
                                issueInputParameters.addCustomFieldValue(cf.getId(), new SimpleDateFormat("d/MMM/yy hh:mm a", Locale.ENGLISH).format(timeStamp));
                                break;
                            case "Group Picker (multiple groups)":
                            case "Group Picker (single group)":
                            case "User Picker (multiple users)":
                            case "Version Picker (single version)":
                                oldstring = oldstring.substring(1, oldstring.length() - 1);
                                issueInputParameters.addCustomFieldValue(cf.getId(), oldstring);
                                break;
                            case "Select List (cascading)":
                                String[] stringArr = oldstring.split("Level 1 values");
                                String parent = StringUtils.substringBetween(stringArr[0], "(", ")");
                                String child = StringUtils.substringBetween(stringArr[1], "(", ")");
                                issueInputParameters.addCustomFieldValue(cf.getId(), parent);
                                issueInputParameters.addCustomFieldValue(cf.getId() + ":1", child);
                                break;
                            case "Select List (multiple choices)":
                            case "Version Picker (multiple versions)":
                            case "Checkboxes":
                                stringArr = oldstring.substring(1, oldstring.length() - 1).replaceAll(" ", "").split(",");
                                issueInputParameters.addCustomFieldValue(cf.getId(), stringArr);
                                break;
                            case "Epic Link Relationship":
                                issueInputParameters.addCustomFieldValue(cf.getId(), data.get("oldstring").toString());
                                break;
                            default:
                                issueInputParameters.addCustomFieldValue(cf.getId(), oldstring);
                                break;
                        }
                    }
                } else {
                    log.warn("field Name: " + fieldName);
                    log.warn("oldstring: " + oldstring);
                    switch (fieldName) {
                        case "duedate":
                            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                            Date date = format.parse(oldstring);
                            issueInputParameters.addCustomFieldValue(fieldName, new SimpleDateFormat("d/MMM/yy hh:mm a", Locale.ENGLISH).format(date));
                            break;
                        case "labels":
                            issueInputParameters.addCustomFieldValue(fieldName, oldstring.split(" "));
                            break;
                        case "status":
                            oldstring = data.get("oldstring").toString();
                            newstring = data.get("newstring").toString();
                            log.error("newstring: " + newstring);
                            log.error("issue.getStatus().getName(): " + issue.getStatus().getName());
                            if (issue.getStatus().getName().trim().equals(newstring.trim())) {
                                log.error("in: " + newstring);
                                IssueWorkflowManager issueWorkflowManager = ComponentAccessor.getComponentOfType(IssueWorkflowManager.class);
                                Collection<ActionDescriptor> availableActions = issueWorkflowManager.getAvailableActions(issue, currentUser);
                                Integer actionId = 0;
                                for (int i = 0; i < availableActions.toArray().length; i++) {
                                    ActionDescriptor curAction = (ActionDescriptor) availableActions.toArray()[i];
                                    if (curAction.getName().contains(oldstring.trim())) {
                                        actionId = curAction.getId();
                                    }
                                }
                                log.warn("actionId: " + actionId);
                                if (actionId > 0) {
                                    IssueService.TransitionValidationResult validationResult = issueService.validateTransition(currentUser, issue.getId(), actionId, issueInputParameters);
                                    if (!validationResult.isValid()) {
                                        log.error("error updateResult: " + validationResult.getErrorCollection().toString());
                                    } else {
                                        IssueService.IssueResult result = issueService.transition(currentUser, validationResult);
                                        log.error("result: " + result);
                                    }
                                }
                            }
                            break;
                        case "Fix Version":
                            fixVerArray = updatedComponentsVersions(fixVerArray, newstring, oldstring);
                            break;
                        case "Version":
                            affectedVerArray = updatedComponentsVersions(affectedVerArray, newstring, oldstring);
                            break;
                        case "Component":
                            compArray = updatedComponentsVersions(compArray, newstring, oldstring);
                            break;
                        case "Link":
                            boolean res = true;
                            String destIssueKey = "";
                            if (newstring == "" && oldstring != "") {//removed
                                destIssueKey = data.get("oldvalue").toString();
                                Issue destIssue = componentAccessor.getIssueManager().getIssueObject(destIssueKey);
                                if (destIssue != null) {
                                    oldstring = data.get("oldstring").toString();
                                    res = handleLinks(destIssue, issue, oldstring, currentUser, false);
                                }
                            } else {//add
                                if (newstring != "" && oldstring == "") {
                                    destIssueKey = data.get("newvalue").toString();
                                    Issue destIssue = componentAccessor.getIssueManager().getIssueObject(destIssueKey);
                                    if (destIssue != null) {
                                        oldstring = data.get("newstring").toString();
                                        res = handleLinks(destIssue, issue, oldstring, currentUser, true);

                                    }
                                }
                            }
                            break;
                        case "timeoriginalestimate":
                            Long originalEstimate = Long.parseLong(oldstring);
                            originalEstimate = originalEstimate / 60;
                            issueInputParameters.setOriginalEstimate(originalEstimate);
                            break;
                        case "timeestimate":
                            Long remainingEstimate = Long.parseLong(oldstring);
                            remainingEstimate = remainingEstimate / 60;
                            issueInputParameters.setRemainingEstimate(remainingEstimate);
                            break;

                        case "timespent":
                            Long timeSpent = Long.parseLong(oldstring);
                            timeSpent = timeSpent / 60;
                            issueInputParameters.setTimeSpent(timeSpent);
                            break;
                        default:
                            issueInputParameters.addCustomFieldValue(fieldName, oldstring);
                            break;
                    }

                }
            } catch (ParseException e) {
                log.error("ParseException: " + e.toString());
            }
            log.warn("issueInputParameters: " + issueInputParameters.getActionParameters());
        }
        IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
        groupIdsArr.add(lastHistoryItems.get(0).getAllFields().get("group").toString().trim());
        // log.warn("groupIdsArr updated: " + groupIdsArr);
        if (!affectedVerArray.equals(affectedVerArrayOrig)){
            issueInputParameters = issueInputParameters.setAffectedVersionIds(affectedVerArray);
        }
        if (!fixVerArray.equals(fixVerArrayOrig)){
            issueInputParameters = issueInputParameters.setFixVersionIds(fixVerArray);
        }
        if (!compArray.equals(compArrayOrig)){
            issueInputParameters = issueInputParameters.setComponentIds(compArray);
        }

        IssueService.UpdateValidationResult validateResult = issueService.validateUpdate(currentUser, issue.getId(), issueInputParameters);
        log.warn("validateResult: " + validateResult.getFieldValuesHolder());

        if (validateResult.isValid()) {
            IssueService.IssueResult updateResult = issueService.update(currentUser, validateResult);
            log.warn("The issue updated successfully: " + issue.getKey());
            List<ChangeHistory> undoChanges = changeHistoryManager.getChangeHistories(issue);
            List<org.ofbiz.core.entity.GenericValue> undoHistoryItems = undoChanges.get(undoChanges.size() - 1).getChangeItems();
            log.warn("undoHistoryItems: " + undoHistoryItems);
            if (!undoHistoryItems.isEmpty()) {
                String undoGroupId = undoHistoryItems.get(0).getAllFields().get("group").toString();
                if (!groupIdsArr.contains(undoGroupId)) {
                    groupIdsArr.add(undoGroupId.trim());
                    log.warn("groupIdsArr: " + groupIdsArr);
                    undoCf.updateValue(null, issue, new ModifiedValue("", groupIdsArr.toString()), changeHolder);
                    ComponentAccessor.getIssueManager().updateIssue(currentUser, (MutableIssue) issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    log.warn("undoCf updated successfully: " + undoCf.getValue(issue));
                    // break;
                } else {
                    log.error("xxx no action to undo! group Ids : "+groupIdsArr+"undo Group Id " +undoGroupId);
                    success = false;
                }
            }
            if (!updateResult.isValid()) {
                success = false;
                log.error("error updateResult: " + updateResult.getErrorCollection().toString());
            }
        } else {
            log.error("error: updateValidationResult" + validateResult.getErrorCollection().toString());
            success = false;
        }
        return success;

    }
    public Long [] getIds(Collection<ProjectComponent> components,Collection<Version> verions){
        List<Long> idsArr=new ArrayList<Long>();
        if (components!=null && !components.isEmpty()){
            for (ProjectComponent val:components) {
                idsArr.add(val.getId());

            }
        }
        if (verions!=null && !verions.isEmpty()){
            for (Version val:verions) {
                idsArr.add(val.getId());

            }
        }
        Long [] array = new Long[idsArr.size()];
        idsArr.toArray(array); // fill the array
        return array;
    }

}